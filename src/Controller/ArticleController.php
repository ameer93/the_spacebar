<?php
/**
 * Created by PhpStorm.
 * User: EngAm
 * Date: 5/30/2018
 * Time: 9:30 PM
 */

namespace App\Controller;


use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class ArticleController extends AbstractController
{

    /**
     * @Route("/", name="app_homepage")
     * @return Response
     */
    public function homepage(): Response
    {
        return $this->render('article/homepage.html.twig');
    }

    /**
     * @Route("news/{slug}", name="article_show")
     * @param $slug
     * @return Response
     */
    public function show($slug, Environment $twigEnvironment): Response
    {
        $title = ucwords(str_replace('-', ' ', $slug));
        $comments = [
            'I ate a normal rock once. It did NOT taste like bacon!',
            'Woohoo! I\'m going on an all-asteroid diet!',
            'I like bacon too! Buy some from my site! bakinsomebacon.com',
        ];

        $html = $twigEnvironment->render('article/show.html.twig', [
            'slug' => $slug,
            'title' => $title,
            'comments' => $comments
        ]);

        return new Response($html);
    }

    /**
     * @Route("/news/{slug}/hearts", name="article_toggle_heart", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function toggleArticleHeart($slug, LoggerInterface $logger)
    {
        $logger->info('The article is hearted');
        return $this->json(['hearts' => rand(1, 50)]);
    }

}